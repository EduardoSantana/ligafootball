namespace LigaFootball.AccesoDatos.Datacontext
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class dbFootballEF : DbContext
    {
        public dbFootballEF()
            : base("name=dbFootballEF")
        {
        }

        public virtual DbSet<Accesos> Accesos { get; set; }
        public virtual DbSet<Archivos> Archivos { get; set; }
        public virtual DbSet<ArchivoTipos> ArchivoTipos { get; set; }
        public virtual DbSet<Categorias> Categorias { get; set; }
        public virtual DbSet<Clubes> Clubes { get; set; }
        public virtual DbSet<ClubesUsuarios> ClubesUsuarios { get; set; }
        public virtual DbSet<Empresas> Empresas { get; set; }
        public virtual DbSet<EmpresasUsuarios> EmpresasUsuarios { get; set; }
        public virtual DbSet<Entidades> Entidades { get; set; }
        public virtual DbSet<Invitaciones> Invitaciones { get; set; }
        public virtual DbSet<Jugadores> Jugadores { get; set; }
        public virtual DbSet<LogCorreos> LogCorreos { get; set; }
        public virtual DbSet<Roles> Roles { get; set; }
        public virtual DbSet<RolesAccesos> RolesAccesos { get; set; }
        public virtual DbSet<RolesUsuarios> RolesUsuarios { get; set; }
        public virtual DbSet<TipoUsuarios> TipoUsuarios { get; set; }
        public virtual DbSet<Tokens> Tokens { get; set; }
        public virtual DbSet<Usuarios> Usuarios { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Accesos>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Accesos>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Accesos>()
                .HasMany(e => e.RolesAccesos)
                .WithRequired(e => e.Accesos)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Archivos>()
                .Property(e => e.Archivo)
                .IsUnicode(false);

            modelBuilder.Entity<Archivos>()
                .Property(e => e.Tipo)
                .IsUnicode(false);

            modelBuilder.Entity<Archivos>()
                .Property(e => e.Extencion)
                .IsUnicode(false);

            modelBuilder.Entity<Archivos>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<ArchivoTipos>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<ArchivoTipos>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Categorias>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Categorias>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Clubes>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Clubes>()
                .Property(e => e.Direccion1)
                .IsUnicode(false);

            modelBuilder.Entity<Clubes>()
                .Property(e => e.Direccion2)
                .IsUnicode(false);

            modelBuilder.Entity<Clubes>()
                .Property(e => e.Direccion3)
                .IsUnicode(false);

            modelBuilder.Entity<Clubes>()
                .Property(e => e.Correo)
                .IsUnicode(false);

            modelBuilder.Entity<Clubes>()
                .Property(e => e.Telefono)
                .IsUnicode(false);

            modelBuilder.Entity<Clubes>()
                .Property(e => e.Celular)
                .IsUnicode(false);

            modelBuilder.Entity<Clubes>()
                .Property(e => e.RNC)
                .IsUnicode(false);

            modelBuilder.Entity<Clubes>()
                .HasMany(e => e.ClubesUsuarios)
                .WithRequired(e => e.Clubes)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Clubes>()
                .HasMany(e => e.Jugadores)
                .WithRequired(e => e.Clubes)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ClubesUsuarios>()
                .Property(e => e.CreadoPor)
                .IsUnicode(false);

            modelBuilder.Entity<Empresas>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Empresas>()
                .Property(e => e.RazonSocial)
                .IsUnicode(false);

            modelBuilder.Entity<Empresas>()
                .Property(e => e.Direccion1)
                .IsUnicode(false);

            modelBuilder.Entity<Empresas>()
                .Property(e => e.Direccion2)
                .IsUnicode(false);

            modelBuilder.Entity<Empresas>()
                .Property(e => e.Direccion3)
                .IsUnicode(false);

            modelBuilder.Entity<Empresas>()
                .Property(e => e.Correo)
                .IsUnicode(false);

            modelBuilder.Entity<Empresas>()
                .Property(e => e.Telefono)
                .IsUnicode(false);

            modelBuilder.Entity<Empresas>()
                .HasMany(e => e.EmpresasUsuarios)
                .WithRequired(e => e.Empresas)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<EmpresasUsuarios>()
                .Property(e => e.CreadoPor)
                .IsUnicode(false);

            modelBuilder.Entity<Entidades>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Entidades>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Entidades>()
                .HasMany(e => e.Archivos)
                .WithRequired(e => e.Entidades)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Invitaciones>()
                .Property(e => e.EnviarA)
                .IsUnicode(false);

            modelBuilder.Entity<Invitaciones>()
                .Property(e => e.Titulo)
                .IsUnicode(false);

            modelBuilder.Entity<Invitaciones>()
                .Property(e => e.Mensaje)
                .IsUnicode(false);

            modelBuilder.Entity<Invitaciones>()
                .Property(e => e.CreadoPor)
                .IsUnicode(false);

            modelBuilder.Entity<Invitaciones>()
                .HasMany(e => e.LogCorreos)
                .WithRequired(e => e.Invitaciones)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<LogCorreos>()
                .Property(e => e.FromMail)
                .IsUnicode(false);

            modelBuilder.Entity<LogCorreos>()
                .Property(e => e.ToMail)
                .IsUnicode(false);

            modelBuilder.Entity<LogCorreos>()
                .Property(e => e.Subject)
                .IsUnicode(false);

            modelBuilder.Entity<LogCorreos>()
                .Property(e => e.Body)
                .IsUnicode(false);

            modelBuilder.Entity<LogCorreos>()
                .Property(e => e.CreadoPor)
                .IsUnicode(false);

            modelBuilder.Entity<LogCorreos>()
                .Property(e => e.Error)
                .IsUnicode(false);

            modelBuilder.Entity<Roles>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Roles>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Roles>()
                .HasMany(e => e.RolesAccesos)
                .WithRequired(e => e.Roles)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Roles>()
                .HasMany(e => e.RolesUsuarios)
                .WithRequired(e => e.Roles)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<RolesAccesos>()
                .Property(e => e.CreadoPor)
                .IsUnicode(false);

            modelBuilder.Entity<RolesUsuarios>()
                .Property(e => e.FechaCreacion)
                .IsUnicode(false);

            modelBuilder.Entity<RolesUsuarios>()
                .Property(e => e.CreadoPor)
                .IsUnicode(false);

            modelBuilder.Entity<TipoUsuarios>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<TipoUsuarios>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Tokens>()
                .Property(e => e.Valor)
                .IsUnicode(false);

            modelBuilder.Entity<Tokens>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Tokens>()
                .Property(e => e.CreadoPor)
                .IsUnicode(false);

            modelBuilder.Entity<Tokens>()
                .HasMany(e => e.LogCorreos)
                .WithRequired(e => e.Tokens)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Usuarios>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Usuarios>()
                .Property(e => e.Apellido)
                .IsUnicode(false);

            modelBuilder.Entity<Usuarios>()
                .Property(e => e.Direccion1)
                .IsUnicode(false);

            modelBuilder.Entity<Usuarios>()
                .Property(e => e.Direccion2)
                .IsUnicode(false);

            modelBuilder.Entity<Usuarios>()
                .Property(e => e.Direccion3)
                .IsUnicode(false);

            modelBuilder.Entity<Usuarios>()
                .Property(e => e.FechaNacimiento)
                .IsUnicode(false);

            modelBuilder.Entity<Usuarios>()
                .Property(e => e.ModificadoPor)
                .IsUnicode(false);

            modelBuilder.Entity<Usuarios>()
                .Property(e => e.Correo)
                .IsUnicode(false);

            modelBuilder.Entity<Usuarios>()
                .Property(e => e.Telefono)
                .IsUnicode(false);

            modelBuilder.Entity<Usuarios>()
                .Property(e => e.Celular)
                .IsUnicode(false);

            modelBuilder.Entity<Usuarios>()
                .Property(e => e.Usuario)
                .IsUnicode(false);

            modelBuilder.Entity<Usuarios>()
                .Property(e => e.Clave)
                .IsUnicode(false);

            modelBuilder.Entity<Usuarios>()
                .HasMany(e => e.ClubesUsuarios)
                .WithRequired(e => e.Usuarios)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Usuarios>()
                .HasMany(e => e.EmpresasUsuarios)
                .WithRequired(e => e.Usuarios)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Usuarios>()
                .HasMany(e => e.Invitaciones)
                .WithRequired(e => e.Usuarios)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Usuarios>()
                .HasMany(e => e.Jugadores)
                .WithRequired(e => e.Usuarios)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Usuarios>()
                .HasMany(e => e.RolesUsuarios)
                .WithRequired(e => e.Usuarios)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Usuarios>()
                .HasMany(e => e.Tokens)
                .WithRequired(e => e.Usuarios)
                .WillCascadeOnDelete(false);
        }
    }
}
