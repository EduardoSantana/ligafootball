namespace LigaFootball.AccesoDatos.Datacontext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class RolesAccesos
    {
        public int RolesAccesosId { get; set; }

        public int RolId { get; set; }

        public int AccesoId { get; set; }

        public DateTime? FechaCreacion { get; set; }

        [StringLength(255)]
        public string CreadoPor { get; set; }

        public virtual Accesos Accesos { get; set; }

        public virtual Roles Roles { get; set; }
    }
}
