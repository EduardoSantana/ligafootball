namespace LigaFootball.AccesoDatos.Datacontext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Invitaciones
    {
        public Invitaciones()
        {
            LogCorreos = new HashSet<LogCorreos>();
        }

        [Key]
        public int InvitacionId { get; set; }

        public int UsuarioId { get; set; }

        [Required]
        [StringLength(255)]
        public string EnviarA { get; set; }

        [Required]
        [StringLength(255)]
        public string Titulo { get; set; }

        [Required]
        [StringLength(255)]
        public string Mensaje { get; set; }

        public DateTime? FechaCreacion { get; set; }

        [StringLength(255)]
        public string CreadoPor { get; set; }

        public virtual Usuarios Usuarios { get; set; }

        public virtual ICollection<LogCorreos> LogCorreos { get; set; }
    }
}
