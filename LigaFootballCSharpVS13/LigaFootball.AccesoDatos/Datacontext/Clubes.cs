namespace LigaFootball.AccesoDatos.Datacontext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Clubes
    {
        public Clubes()
        {
            ClubesUsuarios = new HashSet<ClubesUsuarios>();
            Jugadores = new HashSet<Jugadores>();
        }

        [Key]
        public int ClubId { get; set; }

        [Required]
        [StringLength(255)]
        public string Nombre { get; set; }

        [StringLength(255)]
        public string Direccion1 { get; set; }

        [StringLength(255)]
        public string Direccion2 { get; set; }

        [StringLength(255)]
        public string Direccion3 { get; set; }

        public int? EntidadId { get; set; }

        [StringLength(255)]
        public string Correo { get; set; }

        [StringLength(255)]
        public string Telefono { get; set; }

        [StringLength(255)]
        public string Celular { get; set; }

        [StringLength(255)]
        public string RNC { get; set; }

        public virtual Entidades Entidades { get; set; }

        public virtual ICollection<ClubesUsuarios> ClubesUsuarios { get; set; }

        public virtual ICollection<Jugadores> Jugadores { get; set; }
    }
}
