namespace LigaFootball.AccesoDatos.Datacontext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Tokens
    {
        public Tokens()
        {
            LogCorreos = new HashSet<LogCorreos>();
        }

        [Key]
        public int TokenId { get; set; }

        [Required]
        [StringLength(255)]
        public string Valor { get; set; }

        [Required]
        [StringLength(255)]
        public string Descripcion { get; set; }

        public int UsuarioId { get; set; }

        public DateTime? FechaCreacion { get; set; }

        [StringLength(255)]
        public string CreadoPor { get; set; }

        public DateTime FechaVence { get; set; }

        public virtual ICollection<LogCorreos> LogCorreos { get; set; }

        public virtual Usuarios Usuarios { get; set; }
    }
}
