namespace LigaFootball.AccesoDatos.Datacontext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Empresas
    {
        public Empresas()
        {
            EmpresasUsuarios = new HashSet<EmpresasUsuarios>();
        }

        [Key]
        public int EmpresaId { get; set; }

        [Required]
        [StringLength(255)]
        public string Nombre { get; set; }

        [StringLength(255)]
        public string RazonSocial { get; set; }

        [StringLength(255)]
        public string Direccion1 { get; set; }

        [StringLength(255)]
        public string Direccion2 { get; set; }

        [StringLength(255)]
        public string Direccion3 { get; set; }

        [StringLength(255)]
        public string Correo { get; set; }

        [StringLength(255)]
        public string Telefono { get; set; }

        public int? EntidadId { get; set; }

        public virtual Entidades Entidades { get; set; }

        public virtual ICollection<EmpresasUsuarios> EmpresasUsuarios { get; set; }
    }
}
