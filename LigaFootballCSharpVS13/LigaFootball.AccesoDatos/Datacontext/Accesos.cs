namespace LigaFootball.AccesoDatos.Datacontext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Accesos
    {
        public Accesos()
        {
            RolesAccesos = new HashSet<RolesAccesos>();
        }

        [Key]
        public int AccesoId { get; set; }

        [Required]
        [StringLength(255)]
        public string Nombre { get; set; }

        [StringLength(255)]
        public string Descripcion { get; set; }

        public virtual ICollection<RolesAccesos> RolesAccesos { get; set; }
    }
}
