namespace LigaFootball.AccesoDatos.Datacontext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Jugadores
    {
        [Key]
        public int JugadorId { get; set; }

        public int ClubId { get; set; }

        public int UsuarioId { get; set; }

        public DateTime? FechaCreacion { get; set; }

        [StringLength(255)]
        public string CreadoPor { get; set; }

        public DateTime? FechaModificacion { get; set; }

        [StringLength(255)]
        public string ModificadoPor { get; set; }

        public int? EntidadId { get; set; }

        public virtual Clubes Clubes { get; set; }

        public virtual Entidades Entidades { get; set; }

        public virtual Usuarios Usuarios { get; set; }
    }
}
