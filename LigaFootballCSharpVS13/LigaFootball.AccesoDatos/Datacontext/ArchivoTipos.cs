namespace LigaFootball.AccesoDatos.Datacontext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ArchivoTipos
    {
        public ArchivoTipos()
        {
            Archivos = new HashSet<Archivos>();
        }

        [Key]
        public int ArchivoTipoId { get; set; }

        [Required]
        [StringLength(255)]
        public string Nombre { get; set; }

        [StringLength(255)]
        public string Descripcion { get; set; }

        public virtual ICollection<Archivos> Archivos { get; set; }
    }
}
