namespace LigaFootball.AccesoDatos.Datacontext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ClubesUsuarios
    {
        public int ClubesUsuariosId { get; set; }

        public int ClubId { get; set; }

        public int UsuarioId { get; set; }

        public int? RolId { get; set; }

        public DateTime? FechaCreacion { get; set; }

        [StringLength(255)]
        public string CreadoPor { get; set; }

        public virtual Clubes Clubes { get; set; }

        public virtual Usuarios Usuarios { get; set; }
    }
}
