namespace LigaFootball.AccesoDatos.Datacontext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Entidades
    {
        public Entidades()
        {
            Archivos = new HashSet<Archivos>();
            Clubes = new HashSet<Clubes>();
            Empresas = new HashSet<Empresas>();
            Jugadores = new HashSet<Jugadores>();
            Usuarios = new HashSet<Usuarios>();
        }

        [Key]
        public int EntidadId { get; set; }

        [StringLength(255)]
        public string Nombre { get; set; }

        [StringLength(255)]
        public string Descripcion { get; set; }

        public virtual ICollection<Archivos> Archivos { get; set; }

        public virtual ICollection<Clubes> Clubes { get; set; }

        public virtual ICollection<Empresas> Empresas { get; set; }

        public virtual ICollection<Jugadores> Jugadores { get; set; }

        public virtual ICollection<Usuarios> Usuarios { get; set; }
    }
}
