namespace LigaFootball.AccesoDatos.Datacontext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Roles
    {
        public Roles()
        {
            RolesAccesos = new HashSet<RolesAccesos>();
            RolesUsuarios = new HashSet<RolesUsuarios>();
        }

        [Key]
        public int RolId { get; set; }

        [Required]
        [StringLength(255)]
        public string Nombre { get; set; }

        [StringLength(255)]
        public string Descripcion { get; set; }

        public virtual ICollection<RolesAccesos> RolesAccesos { get; set; }

        public virtual ICollection<RolesUsuarios> RolesUsuarios { get; set; }
    }
}
