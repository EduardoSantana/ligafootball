namespace LigaFootball.AccesoDatos.Datacontext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class LogCorreos
    {
        [Key]
        public int LogCorreoId { get; set; }

        [Required]
        [StringLength(255)]
        public string FromMail { get; set; }

        [Required]
        [StringLength(255)]
        public string ToMail { get; set; }

        [Required]
        [StringLength(255)]
        public string Subject { get; set; }

        [Required]
        [StringLength(255)]
        public string Body { get; set; }

        public DateTime? FechaCreacion { get; set; }

        [StringLength(255)]
        public string CreadoPor { get; set; }

        public bool Enviado { get; set; }

        [StringLength(255)]
        public string Error { get; set; }

        public int TokenId { get; set; }

        public int InvitacionId { get; set; }

        public virtual Invitaciones Invitaciones { get; set; }

        public virtual Tokens Tokens { get; set; }
    }
}
