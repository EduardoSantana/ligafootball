namespace LigaFootball.AccesoDatos.Datacontext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class RolesUsuarios
    {
        public int RolesUsuariosId { get; set; }

        public int RolId { get; set; }

        public int UsuarioId { get; set; }

        [StringLength(255)]
        public string FechaCreacion { get; set; }

        [StringLength(255)]
        public string CreadoPor { get; set; }

        public virtual Roles Roles { get; set; }

        public virtual Usuarios Usuarios { get; set; }
    }
}
