namespace LigaFootball.AccesoDatos.Datacontext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class EmpresasUsuarios
    {
        [Key]
        public int EmpresaUsuariosId { get; set; }

        public int EmpresaId { get; set; }

        public int UsuarioId { get; set; }

        public int? RolId { get; set; }

        public DateTime? FechaCreacion { get; set; }

        [StringLength(255)]
        public string CreadoPor { get; set; }

        public virtual Empresas Empresas { get; set; }

        public virtual Usuarios Usuarios { get; set; }
    }
}
