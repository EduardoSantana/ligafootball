namespace LigaFootball.AccesoDatos.Datacontext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Usuarios
    {
        public Usuarios()
        {
            ClubesUsuarios = new HashSet<ClubesUsuarios>();
            EmpresasUsuarios = new HashSet<EmpresasUsuarios>();
            Invitaciones = new HashSet<Invitaciones>();
            Jugadores = new HashSet<Jugadores>();
            RolesUsuarios = new HashSet<RolesUsuarios>();
            Tokens = new HashSet<Tokens>();
        }

        [Key]
        public int UsuarioId { get; set; }

        [Required]
        [StringLength(255)]
        public string Nombre { get; set; }

        [Required]
        [StringLength(255)]
        public string Apellido { get; set; }

        [StringLength(255)]
        public string Direccion1 { get; set; }

        [StringLength(255)]
        public string Direccion2 { get; set; }

        [StringLength(255)]
        public string Direccion3 { get; set; }

        [Required]
        [StringLength(255)]
        public string FechaNacimiento { get; set; }

        public DateTime? FechaCreacion { get; set; }

        [StringLength(255)]
        public string CreadoPor { get; set; }

        public DateTime? FechaModificacion { get; set; }

        [StringLength(255)]
        public string ModificadoPor { get; set; }

        public int? TipoUsuarioId { get; set; }

        [StringLength(255)]
        public string Correo { get; set; }

        [StringLength(255)]
        public string Telefono { get; set; }

        [StringLength(255)]
        public string Celular { get; set; }

        [StringLength(255)]
        public string Usuario { get; set; }

        [StringLength(255)]
        public string Clave { get; set; }

        public int? EntidadId { get; set; }

        public virtual ICollection<ClubesUsuarios> ClubesUsuarios { get; set; }

        public virtual ICollection<EmpresasUsuarios> EmpresasUsuarios { get; set; }

        public virtual Entidades Entidades { get; set; }

        public virtual ICollection<Invitaciones> Invitaciones { get; set; }

        public virtual ICollection<Jugadores> Jugadores { get; set; }

        public virtual ICollection<RolesUsuarios> RolesUsuarios { get; set; }

        public virtual TipoUsuarios TipoUsuarios { get; set; }

        public virtual ICollection<Tokens> Tokens { get; set; }
    }
}
