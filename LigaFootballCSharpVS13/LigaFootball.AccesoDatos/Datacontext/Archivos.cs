namespace LigaFootball.AccesoDatos.Datacontext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Archivos
    {
        [Key]
        public int ArchivoId { get; set; }

        public int EntidadId { get; set; }

        [Required]
        [StringLength(255)]
        public string Archivo { get; set; }

        [StringLength(255)]
        public string Tipo { get; set; }

        [StringLength(255)]
        public string Extencion { get; set; }

        [StringLength(255)]
        public string Descripcion { get; set; }

        public DateTime? FechaCreacion { get; set; }

        public int? ArchivoTipoId { get; set; }

        public virtual ArchivoTipos ArchivoTipos { get; set; }

        public virtual Entidades Entidades { get; set; }
    }
}
