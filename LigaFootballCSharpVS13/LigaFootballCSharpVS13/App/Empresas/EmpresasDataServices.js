﻿
angularLigaFootballApp.factory('EmpresasDataService',
    ["$http",
    function ($http) {

        var getEmpresas = function () {
            return $http.get("api/Empresas");
        };

        var getEmpresa = function (id) {
            return $http.get("api/Empresas/" + id);
        };

        var insertEmpresa = function (newEmpresa) {
            return $http.post("api/Empresas", newEmpresa);
        };

        var updateEmpresa = function (empresa) {
            return $http.put("api/Empresas/" + empresa.EmpresaId, empresa);
        };

        return {
            insertEmpresa: insertEmpresa,
            updateEmpresa: updateEmpresa,
            getEmpresa: getEmpresa,
            getEmpresas: getEmpresas
        };
    }]);