﻿
angularLigaFootballApp.controller('EmpresasController',
    ["$scope", "$window", "$routeParams", "EmpresasDataService",
    function EmpresasController($scope, $window, $routeParams, EmpresasDataService) {

        if ($routeParams.id)
            $sckope.Empresa = EmpresasDataService.getEmpresa($routeParams.id);
        else
            $scope.Empresa = { id: 0 };

        $scope.editableEmpresa = angular.copy($scope.Empresa);

        $scope.departments = [
            "Engineering",
            "Marketing",
            "Finance",
            "Administration"
        ];

        $scope.shouldShowFullName = function () {
            return true;
        };

        $scope.submitForm = function () {

            $scope.$broadcast('show-errors-event');

            if ($scope.empresaForm.$invalid)
                return;
            
            if ($scope.editableEmpresa.id == 0) {
                // insert new Empresa
                EmpresasDataService.insertEmpresa($scope.editableEmpresa).then(
                    function (results) {
                        // on success
                        //$scope.Empresa = angular.copy($scope.editableEmpresa);
                        //$scope.Empresa.id = results.data.id;
                        $window.history.back();
                    },
                    function (results) {
                        // on error
                        $scope.hasFormError = true;
                        $scope.formErrors = results.statusText;
                    });
            }
            else {
                // update the Empresa
                EmpresasDataService.updateEmpresa($scope.editableEmpresa).then(
                    function (results) {
                        // on success
                        $scope.Empresa = angular.copy($scope.editableEmpresa);
                        $window.history.back();
                    },
                    function (results) {
                        // on error
                        $scope.hasFormError = true;
                        $scope.formErrors = results.statusText;
                    });
            }


        };

        $scope.cancelForm = function () {
            $window.history.back();
        };

        $scope.resetForm = function () {
            $scope.$broadcast('hide-errors-event');
        }

    }]);