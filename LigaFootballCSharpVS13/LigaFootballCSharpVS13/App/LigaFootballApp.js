﻿
var angularLigaFootballApp = angular.module('angularLigaFootballApp', ["ngRoute", "ui.bootstrap"]);

angularLigaFootballApp.config(["$routeProvider", "$locationProvider",
    function ($routeProvider, $locationProvider) {
        $routeProvider
            .when("/home", {
                templateUrl: "Empresas/Index",
                controller: "HomeController"
            })
            .when("/EmpresasCreate", {
                templateUrl: "Empresas/Create",
                controller: "EmpresasController"
            })
            .when("/EmpresasEdit/:id", {
                templateUrl: "Empresas/Edit",
                controller: "EmpresasController"
            })
            .otherwise({
                redirectTo: "/home"
            });
        //$locationProvider.html5Mode(true);
    }]);
