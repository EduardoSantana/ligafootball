﻿
angularLigaFootballApp.controller("HomeController",
    ["$scope", "$location", "EmpresasDataService",
    function ($scope, $location, EmpresasDataService) {

        EmpresasDataService.getEmpresas().then(
            function (results) {
                // on success
                var data = results.data;
            },
            function (results) {
                // on error
                var data = results.data;
            }
        );

        $scope.showEmpresasCreate = function () {
            $location.path('/EmpresasCreate');
        };

        $scope.showEmpresasEdit = function (id) {
            $location.path('/EmpresasEdit/' + id)
        };

    }]);

